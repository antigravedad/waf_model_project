#! /usr/bin/env python
# encoding: utf-8

from waflib.Configure import conf

top = '.'
out = 'build'

def options(opt):
    """Options for compiler"""
    opt.load('compiler_cxx')
    #opt.add_option('--ctags',
    #               help='build ctags options',
    #               action='callback', callback=ctags, dest='ctags', default=False)

@conf
def check_libs(ctx):
    """
    Checks the libraries dependencies
    :param ctx: The configuration context
    :type ctx: waflib.Configure.ConfigurationContext
    """
    ctx.check_cfg(package="libcurl", uselib_store="CURL", mandatory=True)


def configure(conf):
    """configuration previous to build"""
    conf.load('compiler_cxx')

    conf.check_libs()

    #conf.env.gtest_include  = ["/usr/include/gtest"]
    #conf.env.gtest_library  = ["/usr/lib/libgtest.a"]
    #conf.check(features='cxx cxxprogram', lib=['pthread'], uselib_store='PTHREAD')
    #conf.check_cxx(features='cxx cxxprogram', lib = 'gtest_library.a', use = 'gtest_library', cxxflags = '-O2')

def build(bld):
    """Start the build execution"""
    bld.recurse("src")
    # FIXME:need to add gtest compatibility
    #bld.recurse("test")
