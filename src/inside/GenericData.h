#ifndef __GENERIC_DATA__
#define __GENERIC_DATA__

#include <string>
class GenericData {
public:
  GenericData();
  std::string getName();
  void setName(const std::string newName);

private:
  std::string myName;
};

#endif
