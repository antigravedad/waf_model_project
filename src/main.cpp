#include <iostream>
#include <stdio.h>
#include <curl/curl.h>
#include "inside/GenericData.h"

/**
 * Curl example from https://curl.haxx.se/libcurl/c/simple.html
 */
void curlExample()
{
  CURL *curl;
  CURLcode res;

  curl = curl_easy_init();
  if(curl) {
    curl_easy_setopt(curl, CURLOPT_URL, "http://example.com");
    /* example.com is redirected, so we tell libcurl to follow redirection */
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

    /* Perform the request, res will get the return code */
    res = curl_easy_perform(curl);
    /* Check for errors */
    if(res != CURLE_OK)
      fprintf(stderr, "curl_easy_perform() failed: %s\n",
              curl_easy_strerror(res));

    /* always cleanup */
    curl_easy_cleanup(curl);
  }
}

int main(int argc, const char *argv[]) {
  std::cout << "hola mundo" << std::endl;
  GenericData cualquiera;
  cualquiera.setName("nuevo nombre");
  std::cout << cualquiera.getName().c_str() << std::endl;

  curlExample();
  return 0;
}
